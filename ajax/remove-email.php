<?php 

if(!empty($_POST)){
    
    
    $email = $_POST['email'];

    $arr = json_decode(file_get_contents('../json/notif-emails.json'), true);
    
  
    $arr = array_flip($arr);
    
    
    unset($arr[$email]);
    
    $arr = array_flip($arr);
    
    
    file_put_contents('../json/notif-emails.json', json_encode($arr, JSON_UNESCAPED_UNICODE));
    

} else {
    
    echo "Некорректный запрос!";
    
}


?>