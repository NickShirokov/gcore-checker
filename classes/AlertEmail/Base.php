<?php 
//Класс для работы с отправкой Email уведомлений по результатам

namespace AlertEmail;


class Base {
    
    
    public function __construct () {
    
    }
         
    /* Обертка для отправки почты */
    public function sendMailFormSMTP ($to, $subject, $message) {
    	
		$smtp_host = WORK_SMTP_HOST;
		$smtp_user = WORK_SMTP_USER;
		$smtp_pass = WORK_SMTP_PASS;
		$smtp_port = WORK_SMTP_PORT;
		$mail_from = WORK_MAIL_FROM;
		$name_from = WORK_NAME_FROM;
		$mail_reply = WORK_MAIL_REPLY;
	
		
    	date_default_timezone_set('Etc/UTC');
    	$mail = new \PHPMailer;
        //$mail->SMTPDebug = 3;                               // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $smtp_host;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $smtp_user;                 // SMTP username
        $mail->Password = $smtp_pass;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $smtp_port;   
        $mail->CharSet  = 'UTF-8';
        // TCP port to connect to
        
        $mail->setFrom($mail_from, $name_from);
       
         if (is_array($to)) {
            
            foreach ($to as $key => $value) {
                
                $mail->addAddress($value); 
                
                
            } 
            
            
        } else {
            //Set who the message is to be sent to
            $mail->addAddress($to); 
            
        }
       
       
       
        $mail->addReplyTo( $mail_reply, $subject);

        $mail->isHTML(true);                                  // Set email format to HTML
        
        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = $message;
        
        if (!$mail->send()) {
            echo "<br<br>><h2>Mailer Error: " . $mail->ErrorInfo.". <br><br><br>Проверьте настройки SMTP</h2>";

        } else {
            echo "<h1>Message sent!</h1>";
        }
        
        //$mail->send();
    } 
    
    //Метод отправляет мыло обычным способом
    public function sendMailForm ($to, $subject, $message) {
        
        $smtp_host = WORK_SMTP_HOST;
		$smtp_user = WORK_SMTP_USER;
		$smtp_pass = WORK_SMTP_PASS;
		$smtp_port = WORK_SMTP_PORT;
		$mail_from = WORK_MAIL_FROM;
		$name_from = WORK_NAME_FROM;
		$mail_reply = WORK_MAIL_REPLY;
        
        //Create a new PHPMailer instance
        $mail = new \PHPMailer;
        //Set who the message is to be sent from
        $mail->setFrom($mail_from, $name_from);
        //Set an alternative reply-to address
        $mail->addReplyTo( $mail_reply, $subject);
        
        
        if (is_array($to)) {
            
            foreach ($to as $key => $value) {
                
                $mail->addAddress($value); 
                
                
            } 
            
            
        } else {
            //Set who the message is to be sent to
            $mail->addAddress($to); 
            
        }

        
        
        $mail->Port = $smtp_port;   
        $mail->CharSet  = 'UTF-8';
        
        //Set the subject line
        $mail->Subject = $subject;
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $mail->msgHTML($message);
        //Replace the plain text body with one created manually
        $mail->AltBody = $message;
        //Attach an image file
        //$mail->addAttachment('images/phpmailer_mini.png');
        
        //send the message, check for errors
         if (!$mail->send()) {
            echo "<br><br><h2>Mailer Error: " . $mail->ErrorInfo.". <br><br><br> Проверьте настройки почты для отправки результата проверки!</h2>";

        } else {
            echo "<h1>Message sent!</h1>";
        }
    }
        
        
    //Финальный метод отправки сообщения
    public function sendMail ($to, $subject, $message) {
        
        if (SMTP_ACTIVE) {
            $this->sendMailFormSMTP ($to, $subject, $message);
        } else {
             $this->sendMailForm ($to, $subject, $message);
        }
    }
    
    
    
}


?>