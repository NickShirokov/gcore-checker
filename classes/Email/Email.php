<?php 
//Файл для работы с почтой

namespace Email;

use \Engine;
use \AlertEmail;

class Email {
    
    
    public $domObj;
    public $serversArray = array();
    
    public function __construct() {
        //$this->domObj = new \simple_html_dom();
        $this->setServersArray ();
        $this->domObj = Engine\DomSingle::getInstance()->dom_obj;
    }
    
    
    //Список локализаций серверов
    public function setServersArray () {
        
        
        
        
        if (empty($this->serversArray)) {
            $result = json_decode(file_get_contents(FULL_PATH."/servers.json"), true);
            
            foreach ($result as $key => $value) {
                
                if ($value['active'] == "true") {
                    $res[$key] = $value['name'];
                }
            }
            
            $this->serversArray = $res;
        } 
        
        
        
    }
    
    
    //Метод декодирует данные
	public function getDecodeValue($message,$coding)	{
		switch($coding) {
			case 0:
			case 1:
				$message = imap_8bit($message);
				break;
			case 2:
				$message = imap_binary($message);
				break;
			case 3:
			case 5:
			case 6:
			case 7:
				$message = imap_base64($message);
				break;
			case 4:
				$message = imap_qprint($message);
				break;
		}
		
		return $message;
	}


    //Метод получает данные
	public function getData($host, $login, $password, $savedirpath = "") {
		
		
		
		$mbox = imap_open ($host,  $login, $password) or die("<h1>can't connect: " . imap_last_error().". <br><br>Проверьте настройки доступа к email с отчетом! </h1>");
		
		$MC = imap_check($mbox);
		
        // Получим обзор всех писем в INBOX
        //$result = imap_fetch_overview($mbox,"1:{$MC->Nmsgs}",0);
        
        $current_date =  date('d M Y');   
        $some = imap_search($mbox, 'FROM "'.EMAIL_FROM_GCORE_STR.'" SINCE "'.$current_date.'"', SE_UID);
        

        
        
        //Пока юзаем такой костылик
        if (empty($some[0])){
            
            $var = file_get_contents('result_script.txt');
            
            $new_var = (int)$var + 1;
            
            file_put_contents('result_script.txt',$new_var);
            echo "<h2>Письмо на текущую дату $current_date не найдено!</h2>";
            exit;
        }

        $msg_id = $some[0];
        
        $result = imap_fetch_overview($mbox, $msg_id,0);
        

		$message = array();
		$message["attachment"]["type"][0] = "text";
		$message["attachment"]["type"][1] = "multipart";
		$message["attachment"]["type"][2] = "message";
		$message["attachment"]["type"][3] = "application";
		$message["attachment"]["type"][4] = "audio";
		$message["attachment"]["type"][5] = "image";
		$message["attachment"]["type"][6] = "video";
		$message["attachment"]["type"][7] = "other";
		
		
		//Костыль для последнего письма
		$keys = array_keys($result);
        $last = end($keys);
        
        $msg_d = $result[$last];
        
        echo "<h1>#{$msg_d->msgno} ({$msg_d->date}) - From: {$msg_d->from}
        {$msg_d->subject}\n</h1>";

        
		/*---------------------------*/
		//Пробуем преобразовать код для дайльнейшего использования
		$structure = imap_fetchstructure($mbox, $msg_id );    
		$parts = $structure->parts;
		
		
		$res = '';
		
		
		
	    foreach ($parts as $key => $value) {
	        
	        if(!empty($value->disposition) && $value->disposition == 'attachment') {
	            
	            $mege = "";
				$data = "";
			  	$mege = imap_fetchbody($mbox, $msg_id, 2);  
			  	
                //Получаеv html из файла
                $html = imap_base64($mege);
                //echo $html;
                
                $res.= $html;
	        }
	    }

		imap_close($mbox);


		return $res;
	}
    
    
    //Метод парсит всю схему вложения и возвращает текущий массив
    public function parseAttachment () {
        //Создаем объект парса и получаем сам html код
        $domObj = $this->domObj;
        $html_content = $this->getData (EMAIL_HOST, EMAIL_LOGIN, EMAIL_PASSWORD);
        
        $domObj->load($html_content);
        
        //Получаем заголовки, проходим по ним циклом для вытаскивания содержимого и кладем их в массив
        $headers = $domObj->find('h2');
        
        
        $cur_parse_h2 = array();
        
        foreach ($headers as $key => $value) {
            
            
            
            
            //Здесь мы должны проверить
            if (in_array( $value->innertext, $this->serversArray)) {
                
                $cur_parse_h2[$value->innertext] = $value;
            }
        }
        
        
        
        
        
        $result = array ();
        

        
        //Далее мы должны все это распарсить и положить в финальный массив
        foreach ($cur_parse_h2 as $key => $value) {
            
            $datacenter = $key;
            
            $next = $value->nextSibling();
            $td_conf_first = $next->find('.mainBody .b-table__col_first');
            $td_conf_last = $next->find('.mainBody .b-table__col_last');
            
            //Циклом пишем значения по ячейкам
            foreach ($td_conf_first as $key => $value) {
                //Проверянм на пустые, потому что в тексте письма такие бывали
                if (!empty($value->innertext)) {
                    $result[$datacenter][] = array (
                                'name' => $value->innertext,
                                'value' => $td_conf_last[$key]->innertext
                                );
                }
            }
            
        }
        
        
        
        return $result;
    }
    
    
    
}






?>
