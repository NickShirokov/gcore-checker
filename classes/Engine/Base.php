<?php 
//Базовый класс для работы 

namespace Engine;

use \AlertEmail;

class Base {
    
    
    public $currentCookie;
    public $serversArray = array();
    public $baseUrl;
    
    public $domObj;
    
    
    public function __construct(){
        //$this->setCookie();
        $this->setServersArray ();
        $this->domObj = DomSingle::getInstance()->dom_obj;
    }
    //Метод ставит урл для получения куки
    public function initUrl ($url) {
        $this->baseUrl = $url;
        
        
    }
    
    //Список локализаций серверов
    public function setServersArray () {
        if (empty($this->serversArray)) {

            $result = json_decode(file_get_contents(FULL_PATH."/servers.json"), true);
            
            if (empty($this->serversArray)) {
               $result = json_decode(file_get_contents(FULL_PATH."/servers.json"), true);
                
                foreach ($result as $key => $value) {
                    
                    if ($value['active'] == "true") {
                        $res[$key] = $value['name'];
                    }
                }
                
                
                $this->serversArray = $res;
            } 
            
            
        } 
        
    }
    

    //Устанавливает куку
    public function setCookie() {
        
        
        $out = curl_get($this->baseUrl);
        
            
        //Отрезаем сначала искомую строку
        $re = '/billmgrses5=*.*/';
        preg_match_all($re, $out, $matches, PREG_SET_ORDER, 0);

        // Print the entire match result
       
        
        
        if (!empty($matches[0][0])) {
            
            
            $new_str = $matches[0][0];
            
            $re = '/=(.*.);/XU';
            
            preg_match($re, $new_str, $matches, PREG_OFFSET_CAPTURE, 0);
            
            
            // Print the entire match result
            $ccook = $matches[1][0];
            //Получаем значение куки
            echo "Current cookie to auth: " . $ccook;
            $this->currentCookie = $ccook;
    
            if (empty($ccook)) {
                
                
                echo "Не удалось получить cookie авторизации! Проверьте конфиги!";
                
                $alert_obj = new AlertEmail\Base ();
    			
    			$alert_obj->sendMail (EMAIL_OUTPUT, "Результаты проверки", "<h2>Не удалось получить cookie авторизации! Проверьте конфиги!</h2>");
    
                exit;
            }
        
        } else {
            
            echo "Не удалось получить cookie авторизации! Проверьте конфиги!";
                
                
    
                
            $alert_obj = new AlertEmail\Base ();
			
			$alert_obj->sendMail (EMAIL_OUTPUT, "Результаты проверки", "<h2>Не удалось получить cookie авторизации! Проверьте конфиги!</h2>");

            exit;
            
            
            
        }

    }
    
    
    //Возвращает нужную нам куку
    public function getCookie() {
        
        return $this->currentCookie;
    }
    
    //Метод получает из апи данные
    public function getDataApi ($url, $cook) {
        $result = curl_get_with_cook ($url, $cook);
        $result = json_decode($result, true);
        
        return $result;
    }
    
    //Метод разбирает массив данных из АПИ и кладет в удобный массив
    public function getResultApiArray ($url, $cook) {
        
        $domObj = $this->domObj;
        
        //Получаем данные из АПИ и обрабатывем их
        $res = $this->getDataApi ($url, $cook);
        
        if (!empty($res['doc']['list'][0]['elem'])) {
            $list_servers = $res['doc']['list'];
            
            
            
            $elem_arr = $list_servers[0]['elem'];
            $res_arr = array();
            
            //Проходим циклом и кладем в массив
            foreach ($elem_arr as $key => $value) {
                
                $server_html = $value['desc']['$'];
                
                $domObj->load($server_html);
                $aa = $domObj->find('div');
                
                $res_arr[] = $aa[0]->innertext;
                
            }
            
           return $res_arr; 
        } else {
            return array();
        }
        
    }
    
    
    
    
}



?>
