<?php 
//Пробуем одиночку для Simple Dom Object
namespace Engine;

class DomSingle {
    
    //Какие-то настройки
    private $settings = array();
    //Перемнная, в которую кладем объект для использования
    public $dom_obj;
    
    private static $_instance = null;
    
    private function __construct() {
        // приватный конструктор ограничивает реализацию getInstance ()
        $this->dom_obj = new \simple_html_dom();
    }
    
    protected function __clone() {
        // ограничивает клонирование объекта
    }
    
    static public function getInstance() {
        if(is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    
    
} 

?>