<?php 
//Класс для вывода результатов ва HTML формате - пока только для 
//глобального конфига

namespace Results;


class ResultsBase {
    
    public function __construct () {
        
        
        
    }
    
    
    
    public function resultNullBalance ($name) {
        
        $content = '';
        if (NULL_BALANCE) {
        
             $content.= "<td border=1 style='border: 1px solid #000'>$name </td> <td style='border: 1px solid #000'>".NULL_BALANCE_TEXT."</td>";
        }

        return $content;
    }
    
    public function resultSuccess ($name) {
        
        $content = '';
        
        if (SUCCESS_CHECK) {
            $content.= "<td border=1 style='border: 1px solid #000'>$name </td> <td style='border: 1px solid #000' border=1>".SUCCESS_CHECK_TEXT."</td>";
        }


        return $content;
    }
    
    public function resultBalanceCheck ($name) {
        
        $content = '';
        if (BALANCE_CHECK) {
            $content.= "<td border=1 style='border: 1px solid #000'>$name </td> <td border=1 style='border: 1px solid #000'>".BALANCE_CHECK_TEXT."</td>";
        }

        return $content;
    }
    
    public function resultError ($name) {
        
        $content = '';
        if ( INVALID_KEY ) {
            $content.= "<td border=1 style='border: 1px solid #000'>$name</td> <td border=1 style='border: 1px solid #000'>".INVALID_KEY_TEXT."</td>";
        }

        return $content;
    }
    
    
    
    //Метод для вывода таблицы с результатами
    public function resultsTable (array $result_attach, array $result) {
        
        $content = '';
        
        //Финальный обход
        foreach ($result_attach as $key => $value) {
            
            $datacenter = $key;
            $datacenter_arr = $value;


               
            $head = "<h3>$key</h3>";

            $tb_h = '<table>
                            <tbody>';
            $content_b = '';
            //Идем циклом по внутреннему массиву, содлежащему серваки
            foreach ($datacenter_arr as $key => $value) {
                
                $content_a = '';
                
                $name = $value['name'];
                $count = $value['value'];
                
                if (!in_array($name, $result[$datacenter]) && $count == 0) {
                    $content_a.= $this->resultNullBalance ($name) ;
                } elseif (in_array($name, $result[$datacenter]) && ($count < COUNT_BALANCE)) {
                    $content_a.= $this->resultBalanceCheck ($name);
                } elseif (in_array($name, $result[$datacenter]) && ($count >= COUNT_BALANCE)) { 
                    $content_a.= $this->resultSuccess ($name);
                } else {
                    $content_a.= $this->resultError ($name);
                }
            
                if (!empty ($content_a)) {
                    $content_b.= "$tb_h<tr>".$content_a."</tr></tbody></table>";
                }
                
            } 
            
            if (!empty($content_b)) {
                $content.= $head.$content_b;
            }
            
            

        } 
        
        return $content;
    }
    
    
    
      
    
    //Метод для вывода таблицы с результатами для нотификация
    public function resultsTableNotif (array $result_attach, array $result, array $notif_arr) {
        
        $content = '';
        
        
        //Пройдем по массиву нотификация и положим ключи
        foreach ($notif_arr as $key => $value) {
            
            
            $name = $value['name'];
            
            $new_notif[$name] = $value; 
            
        }
        
        
        
        
        //Финальный обход
        foreach ($result_attach as $key => $value) {
            
            $datacenter = $key;
            $datacenter_arr = $value;


               
            $head = "<h3>$key</h3>";

            $tb_h = '<table>
                            <tbody>';
            $content_b = '';
            
            
            //Идем циклом по внутреннему массиву, содлежащему серваки
            foreach ($datacenter_arr as $key => $value) {
                
                $content_a = '';
                
                $name = $value['name'];
                $count = $value['value'];
                
                
                //Проверяем наличие м новом notif_arr
                
                if (array_key_exists($datacenter, $new_notif)) {
                    
                    if (//in_array($name, $result[$datacenter]) 
                         !empty($new_notif[$datacenter]['data'])
                        && array_key_exists($name, $new_notif[$datacenter]['data'])
                        && $count < $new_notif[$datacenter]['data'][$name]['count']
                    ) {
                        
                        
                        $than_count = $new_notif[$datacenter]['data'][$name]['count'];
                        
                        $content_a.= "<td border=1 style='border: 1px solid #000'>$name </td> <td border=1 style='border: 1px solid #000'>Less than $than_count</td>
                        <td>$count</td>";
                        
                    }
                    
                    
                }
                
                
            
                if (!empty ($content_a)) {
                    $content_b.= "$tb_h<tr>".$content_a."</tr></tbody></table>";
                }
                
            } 
            
            if (!empty($content_b)) {
                $content.= $head.$content_b;
            }
            
            

        } 
        
        return $content;
    }
    
    
    
    
      
    //Метод для вывода таблицы с результатами для нотификация
    public function resultsTableRegNotif (array $result_attach, array $result, array $notif_arr) {
        
        
        
        $content = '';
        
        
         $res_reg_arr = array();
        
        //Пройдем по массиву нотификация и положим ключи
        foreach ($notif_arr as $key => $value) {
            
            
            
            
            
            $reg = $key;
            
            
            $reg_arr = explode(' ', $reg );
            
            
                
            $centers = $value['centers'];
            
            $count = $value['count'];
           
            
           foreach ($reg_arr as $key => $value) {
               
               
                if(!empty($value))  {
                    
                    
                     $sub_reg = $value;

        
            
            //ПРойдем циклом по центам для регулярки нотификация
            foreach ($centers as $key => $value) {
                
                $center_num = $value;
                
                $servers_arr = $result_attach[$center_num];
                
                
                //Ищем совпадения на регулярку
                if (!empty($servers_arr)) {
                    
                    foreach ($servers_arr as $key => $value) {
                        
                        if(stristr($value['name'], $sub_reg)) {
                            //$res_reg_arr[$reg]['name'] = $reg; 
                            if (!empty($res_reg_arr[$reg][$center_num]['summa'])) {
                                
                                $res_reg_arr[$reg][$center_num]['summa']+= $value['value']; 
                            } else {
                                $res_reg_arr[$reg][$center_num]['summa'] = $value['value'];
                        
                            }
                        } 
                        
                    }
                    
                }
                
            }
            
        }
        
           }
        
        }
        //Далее формируем табличку


      
        $content.= '<h2>Regexp Notification "Number of remaining servers"</h2>';

        
        foreach ($res_reg_arr as $key => $value) {
            
            $cent_arr = $value;

            
            $count = $notif_arr[$key]['count'];
          
                                
             

            $name_cond = '';
            
            if (!empty($notif_arr[$key]['name'])) {
                $name_cond = $notif_arr[$key]['name'];
                
            }
            //$content.= '<h3>'.$key.'</h3>';
            
            $table_h = '<h3>'.$name_cond.'</h3>
                        <p>'.$key.'</p>';
            
            $table_h.= '<table>
                            <thead>
                                <th>Datacenter</th>
                                
                                <th>Summa</th>
                                <th>Less</th>
                            </thead>
                            <tbody>';
                            
                            
            $table_f =    '</tbody>
                        </table>';
                    
            
            
            $table_b = '';        
                    
            
            
            foreach ($cent_arr as $key => $value) {
                
                if ($value['summa'] < $count) {
                
                    $table_b.= '<tr>
                    
                                    <td border=1 style="border: 1px solid #000">'.$key.'</td>
                                    <td border=1 style="border: 1px solid #000">'.$value['summa'].'</td>
                                    <td border=1 style="border: 1px solid #000">Less than'.$count.'</td>
                                        
                                </tr>';
                            
                }
            }
            
            
            
            if (!empty($table_b)) {
                
                
                $content.=$table_h.$table_b.$table_f;
            } 
            
            /*
            
            $content.= '<table>
                            <thead>
                                <th>Datacenter</th>
                                
                                <th>Summa</th>
                                <th>Less</th>
                            </thead>
                            <tbody>';
                            
                foreach ($cent_arr as $key => $value) {
                    
                    if ($value['summa'] < $count) {
                    
                        $content.= '<tr>
                        
                                        <td border=1 style="border: 1px solid #000">'.$key.'</td>
                                        <td border=1 style="border: 1px solid #000">'.$value['summa'].'</td>
                                        <td border=1 style="border: 1px solid #000">Less than'.$count.'</td>
                                            
                                    </tr>';
                                
                    }
                }
                            
            $content.=      '</tbody>
                        </table>';
                    
              */      

                
        }
 
 
        
        return $content;
    }
    
    
    
}



?>
