<?php include('html/header.php'); ?>


<section>
    
    <div class="container">
        <h2>Основная конфигурационная настройка (для алертов на мыло после полной проверки)</h2>
        
        
        <div class="col-md-6">
        <form class="form-signin" action="form_call/form1.php" method="POST">
            
            
            <?php 
            
            $arr = file_get_contents('json/1.json');
            $arr = json_decode($arr, true);
            
            ?>
            
            
            
            <h3>Настройки для получения репорта</h3>
            <label>EMAIL HOST</label>
            <input class="form-control" type="text" name="EMAIL_HOST" value="<?php echo $arr['EMAIL_HOST']; ?>" placeholder="host">
            
            <label>EMAIL LOGIN</label>
            <input class="form-control" type="text" value="<?php echo $arr['EMAIL_LOGIN']; ?>" name="EMAIL_LOGIN" placeholder="login">
            
            <label>EMAIL PASSWORD</label>
            <input class="form-control" type="password" name="EMAIL_PASSWORD" value="<?php echo $arr['EMAIL_PASSWORD']; ?>" placeholder="password">
            
            <label>EMAIL FROM GCORE STR </label>
            <input class="form-control" type="text"  value="<?php echo $arr['EMAIL_FROM_GCORE_STR']; ?>" name="EMAIL_FROM_GCORE_STR" placeholder="from string">
            
            <button class="btn btn-lg btn-primary btn-block">Save</button>
        </form>
        
        </div>
        
             
             
        <div class="col-md-6">     
                
        <form class="form-signin" action="form_call/form2.php" method="POST">
            
            
             <?php 
            
            $arr = file_get_contents('json/2.json');
            $arr = json_decode($arr, true);
            
            ?>
            
            
            
            
            <h3>URL адреса билингов</h3>
            <label>EURO</label>
            <input class="form-control" name="G_CORE_DOMAIN" value="<?php echo $arr['G_CORE_DOMAIN'];?>" type="text" placeholder="G_CORE_DOMAIN">
            
            <label>RU</label>
            <input class="form-control" name="G_CORE_DOMAIN_RU" type="text" value="<?php echo $arr['G_CORE_DOMAIN_RU'];?>" placeholder="G_CORE_DOMAIN_RU">
            
            <label>DOLLAR</label>
            <input class="form-control" type="text" name="G_CORE_DOMAIN_US" value="<?php echo $arr['G_CORE_DOMAIN_US'];?>" placeholder="G_CORE_DOMAIN_US">
            <button class="btn btn-lg btn-primary btn-block">Save</button>
        </form>
        
        </div>
        
        <div class="clear"></div>



         <div class="col-md-6">     
                
        <form class="form-signin" action="form_call/form3.php" method="POST">
            
            
             <?php 
            
            $arr = file_get_contents('json/3.json');
            $arr = json_decode($arr, true);
            
            $check_smtp_active = '';
            
            if ($arr['SMTP_ACTIVE']) {
                $check_smtp_active="checked";
            }
            
            
            ?>
            
            
            <h3>SMTP config для отправки уведомлений</h3>
            <div>
            <!-- <input type="checkbox" id="smtp_active" name="SMTP_ACTIVE" value="<?php echo $arr['SMTP_ACTIVE'];?>">
        -->
                
        
                 <input type="checkbox" id="smtp_active" name="SMTP_ACTIVE" <?php echo $check_smtp_active; ?> value="true">
        
        
            <label for="smtp_active">SMTP ACTIVE</label>
            </div>
            
            
            <label>WORK_SMTP_HOST</label>
            <input class="form-control" type="text" name="WORK_SMTP_HOST" value="<?php echo $arr['WORK_SMTP_HOST'];?>" placeholder="WORK_SMTP_HOST">
            
            <label>WORK_SMTP_USER</label>
            <input class="form-control" type="text" value="<?php echo $arr['WORK_SMTP_USER'];?>" name="WORK_SMTP_USER"  placeholder="WORK_SMTP_USER">
            
            <label>WORK_SMTP_PASS</label>
            <input class="form-control" type="password" value="<?php echo $arr['WORK_SMTP_PASS'];?>" name="WORK_SMTP_PASS" placeholder="WORK_SMTP_PASS">
            
            
            
            <label>WORK_SMTP_PORT</label>
            <input class="form-control" type="text" value="<?php echo $arr['WORK_SMTP_PORT'];?>" name="WORK_SMTP_PORT" placeholder="WORK_SMTP_PORT">
            
            <label>WORK_MAIL_FROM</label>
            <input class="form-control" type="text" value="<?php echo $arr['WORK_MAIL_FROM'];?>" name="WORK_MAIL_FROM" placeholder="WORK_MAIL_FROM">
            
            <label>WORK_NAME_FROM</label>
            <input class="form-control" type="text" value="<?php echo $arr['WORK_NAME_FROM'];?>" name="WORK_NAME_FROM" placeholder="WORK_NAME_FROM">
            
           
             
            <label>WORK_MAIL_REPLY</label>
            <input class="form-control" type="text" value="<?php echo $arr['WORK_MAIL_REPLY'];?>" name="WORK_MAIL_REPLY" placeholder="WORK_MAIL_REPLY">
           
           
            
            <button class="btn btn-lg btn-primary btn-block">Save</button>
        </form>
        
        </div>


        
        
        
         <div class="col-md-6">     
                
        <form class="form-signin" action="form_call/form4.php" method="POST">
            
            
               
        <?php 
            
            $arr = file_get_contents('json/4.json');
            $arr = json_decode($arr, true);
            
            $check_1 = '';
            
            $check_2 = '';
            
            $check_3 = '';
            
            $check_4 = '';
            
            if ($arr['SUCCESS_CHECK']) {
                $check_1 = "checked";
            }
        
        
             if ($arr['NULL_BALANCE']) {
                $check_2 = "checked";
            }
            
             if ($arr['BALANCE_CHECK']) {
                $check_3 = "checked";
            }
            
             if ($arr['INVALID_KEY']) {
                $check_4 = "checked";
            }
        
        ?>
            
            
            
            
            <h3>Настройки проверки для уведомлений</h3>
            
            <label>EMAIL_OUTPUT</label>
            <input class="form-control" type="text" name="EMAIL_OUTPUT" value="<?php echo $arr['EMAIL_OUTPUT'];?>" placeholder="EMAIL_OUTPUT">
            
            
            <div>
             <input type="checkbox" id="smtp_active" <?php echo $check_1; ?>  name="SUCCESS_CHECK" value="true">
                <label for="smtp_active">SUCCESS_CHECK</label>
            </div>
            
            <label>SUCCESS_CHECK_TEXT</label>
            <input class="form-control" type="text" name="SUCCESS_CHECK_TEXT" value="<?php echo $arr['SUCCESS_CHECK_TEXT'];?>" placeholder="SUCCESS_CHECK_TEXT">
            
            
            
             <div>
             <input type="checkbox" id="smtp_active"  <?php echo $check_2; ?>  name="NULL_BALANCE" value="true">
            <label for="smtp_active">NULL_BALANCE</label>
            </div>
            

            
            <label>NULL_BALANCE_TEXT</label>
            <input class="form-control" type="text" name="NULL_BALANCE_TEXT" value="<?php echo $arr['NULL_BALANCE_TEXT'];?>" placeholder="NULL_BALANCE_TEXT">
            
            
             <div>
             <input type="checkbox" id="smtp_active" name="BALANCE_CHECK"  <?php echo $check_3; ?>  value="true">
            <label for="smtp_active">BALANCE_CHECK</label>
            </div>
            
            
            
            
            
            <label>COUNT_BALANCE</label>
            <input class="form-control" type="text" name="COUNT_BALANCE" value="<?php echo $arr['COUNT_BALANCE'];?>" placeholder="COUNT_BALANCE">
            
            <label>BALANCE_CHECK_TEXT</label>
            <input class="form-control" type="text" name="BALANCE_CHECK_TEXT" value="<?php echo $arr['BALANCE_CHECK_TEXT'];?>" placeholder="BALANCE_CHECK_TEXT">
            
            <div>
             <input type="checkbox" id="smtp_active" name="INVALID_KEY"  <?php echo $check_4; ?> value="true">
            <label for="smtp_active">INVALID_KEY</label>
            </div>
            
            
             
            <label>INVALID_KEY_TEXT</label>
            <input class="form-control" type="text" name="INVALID_KEY_TEXT" value="<?php echo $arr['INVALID_KEY_TEXT'];?>" placeholder="INVALID_KEY_TEXT">
           
           
            
            <button class="btn btn-lg btn-primary btn-block">Save</button>
        </form>
        
        </div>



        
        
        
    </div>
    
    
</section>




<?php include('html/footer.php'); ?>