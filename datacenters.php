<?php 

include('html/header.php'); 


?>



<section>
    
    <div class="container">
        <h2>Датацентры</h2>
        
        
        <form class="form-signin" action="form_call/form_datacenters_edit.php" method="POST">
        <h3>Настройки для центров</h3>
        
        <?php 
        
            $content = '';
            $res = json_decode(file_get_contents('servers.json'), true);
  
        
            foreach ($res as $key => $value) {
                
                $checked = '';
                $active = "";
                
                if ($value['active'] == 'true') {
                    
                    
                    $checked = 'checked';
                    $active = "true";
                    
                } 
                
                $content.= '
                    <div class="col-md-1">
                    <input type="text" class="form-control" name="index['.$key.'][index]" value="'.$key.'">
                    </div>
                    
                    <div class="col-md-4">
                    <input type="text" class="form-control" name="index['.$key.'][name]" value="'.$value['name'].'">
                    </div>
                    
                    <div class="col-md-1">
                    <input type="checkbox" class="form-control" name="index['.$key.'][active]" '.$checked.' value="true">
                    </div>
                    
                    <div class="clear"></div>
                    ';
            }
            
            
            echo $content;           
        ?>

          <button class="btn btn-lg btn-primary">Save</button>
        
        </form>


        
        
         <form class="form-signin" action="form_call/form_datacenters_new.php" method="POST"> 
         
             <h3>Добавить центр</h3>
             
             
              <div class="col-md-2">
            <input type="number" class="form-control" name="number" value="">
            </div>
             
             <div class="col-md-4">
            <input type="text" class="form-control" name="name" value="">
            </div>
            
            <div class="col-md-1">
            <input type="checkbox" class="form-control" name="active" checked value="true">
            </div>
            
            <div class="clear"></div>
             
         
         <button class="btn btn-lg btn-primary">Добавить</button>
         </form>
        

          
         <form class="form-signin" action="form_call/form_datacenters_remove.php" method="POST"> 
         
             <h3>Удалить центр (если нужно??)</h3>
             
             
            <div class="col-md-2">
            <input type="number" class="form-control" name="number" value="">
            </div>
             
           
            
            <div class="clear"></div>
             
         
         <button class="btn btn-lg btn-primary">Удалить</button>
         </form>
        



    </div>
    
    
    
</section>



<?php include('html/footer.php');  ?>