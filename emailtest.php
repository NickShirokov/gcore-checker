<?php
//
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

//header('Content-Type: application/json');
require_once ('init.php');

include ('html/header.php');

//Создаем объект класса для авторизации и получаем куку для работы
$obj = new Engine\Base();

$var = G_CORE_DOMAIN.'/billmgr?func=showroom.redirect&newwindow=yes&redirect_to=dedic.order.pricelist%26out%3Dxml%26datacenter%3D5';
$obj->initUrl ($var);




$result = array();


//Получаем массив с данными из письма
$email_obj = new Email\Email();
$result_attach = $email_obj->parseAttachment ();


//Пробуем привязать нотификации
$notif_arr = json_decode(file_get_contents(FULL_PATH.'/json/reg-notif.json'),true);


$datacenters = json_decode(file_get_contents(FULL_PATH.'/servers.json'),true);

$new_arr = array();
//Переложили ключи
foreach ($datacenters as $key => $value) {
    $name = $value['name'];
    $new_arr[$key] = $result_attach[$name];
    
}


//Пробуем привязать нотификации
$notif_arr = json_decode(file_get_contents(FULL_PATH.'/json/notif-servers.json'),true);


//Пробуем привязать нотификации
$notif_arr_reg = json_decode(file_get_contents(FULL_PATH.'/json/reg-notif.json'),true);



//Собираем HTML Вывод
$result_object = new Results\ResultsBase ();


$html_result_content = $result_object->resultsTableNotif($result_attach, $result, $notif_arr);



$html_result_reg =  $result_object->resultsTableRegNotif($new_arr, $result, $notif_arr_reg);




$style = "<style>


    td {
        width: 50%;
        border: 1px solid #000;
        padding: 3px;
    } 


</style>";




$html_output = 

"<html>
    <head>".$style."</head>

    <body>
    
    ".$html_result_content.$html_result_reg."
        </body>


</html>";


echo $html_output;

$alert_obj = new AlertEmail\Base ();


$mail_arr = json_decode(file_get_contents(FULL_PATH.'/json/notif-emails.json'),true);

//Если вывод проверки скрипта пустой, то мыло не отправляем
if (!empty($html_result_content)) {
    $alert_obj->sendMail ($mail_arr, "Regexp Notification \"Number of remaining servers\"", $html_output);
} else {
    echo "Проверка прошла успешно!";
    $alert_obj->sendMail ($mail_arr, "Regexp Notification \"Number of remaining servers\"", 'Check success!');
     
}






include('html/footer.php');

?>
