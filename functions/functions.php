<?php 
//Новый файл для функций 

function curl_get ($url) {

    if( $curl = curl_init() ) {    
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        $out = curl_exec($curl);
        
        
        return $out;
    }  else {
        echo "Не удалось получить доступ к серверу АПИ!";
        exit;    
    }
}


function curl_get_with_cook ($url, $cook) {

    if( $curl = curl_init() ) {
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    
        curl_setopt($curl, CURLOPT_COOKIE, "billmgrses5=".$cook);
        $result_json = curl_exec($curl);
        
        
        //print_r (curl_getinfo($curl));
        //exit;
        
        curl_close($curl);
        
        return $result_json;
        
    } else {
        echo "Не удалось получить доступ к серверу АПИ!";
        exit;    
    }
}



//Строит дерево html элементов в массив
function element_to_obj($element) {

    $obj = array( "tag" => $element->tagName );

    foreach ($element->attributes as $attribute) {
        $obj[$attribute->name] = $attribute->value;
    }
    
     
    foreach ($element->childNodes as $subElement) {
        if ($subElement->nodeType == XML_TEXT_NODE) {
            $obj["html"] = $subElement->wholeText;
        }
        else {
            $obj["children"][] = element_to_obj($subElement);
        }
    }

    return $obj;
}


//Формирует массив для парсинга html
function html_to_obj($html) {
    $dom = new DOMDocument();
    $dom->loadHTML($html);
    return element_to_obj($dom->documentElement);
}




?>