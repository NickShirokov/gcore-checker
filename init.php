<?php 
//Файл инициализации
require_once('config.php');



//Здесь делаем парс json файлов для define конфига (иначе включаем settings)

if (is_file('json/1.json')
    && is_file('json/2.json')
    && is_file('json/3.json')
    && is_file('json/4.json')
) {
    
    for ($i = 1; $i <= 4; $i++){
    
        $arr = json_decode(file_get_contents('json/'.$i.'.json'),true);
    
        foreach ($arr as $key => $value) {
            
            define($key, $value);
            
        } 
  
    }
} else {

    require_once ('settings.php');
}



include('lib/simple_html_dom.php');
require_once ('functions/functions.php');
require 'PHPMailer/PHPMailerAutoload.php';

require_once __DIR__ . DIRECTORY_SEPARATOR . 'Autoloader' . DIRECTORY_SEPARATOR . 'NamespaceAutoloader.php';


date_default_timezone_set('Europe/Moscow');



$autoloader = new NamespaceAutoloader();
$autoloader->addNamespace('Engine', __DIR__ . '/classes/Engine');
$autoloader->addNamespace('Email', __DIR__ . '/classes/Email');
$autoloader->addNamespace('AlertEmail', __DIR__ . '/classes/AlertEmail');

$autoloader->addNamespace('Results', __DIR__ . '/classes/Results');
//$autoloader->addNamespace('Api', __DIR__ . '/classes/Api');
$autoloader->register();

session_start();


?>