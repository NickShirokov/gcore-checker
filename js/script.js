
//Файл для работы с нотификациями

$(document).ready(function() {
    
    removeEmail ();
    
    
    removeNotif ();
    
    
    removeRegNotif ();
    
});





function newWindowRedirect(url) {	
	window.open(url,'name','width=990,height=630,left=300,top=100,scrollbars=0'); 	
}

function windowRef(url){ 		
	window.location = url;
} 


function urlWithProtocol () {
	var url = window.location.protocol+"//"+window.location.host;	
	return url;
}




function addEmail () {
    
    
    var url = urlWithProtocol ();
    
    var msg = jQuery('#add-email').serialize();

	$.ajax({
		type: "POST",          
		url: url+"/ajax/email.php",
		data: msg,
		success: function(data) {
		    
		    
		    $('.emails-box ul').append('<li data-val='+data+'>'+data+'<span class="remove">X</span></li>');
            
            $('#email').val('');
            
            removeEmail ();
		},
		error: function () {
		    
		    alert("Произошла ошибка!");    
		    
		}
	});

}


function removeEmail () {
    
    
    $('.emails-box .remove').click(function(){
        
          var url = urlWithProtocol ();
        
        email = $(this).parent().attr('data-val');
        
        
        li_el = $(this).parent();
        
        
        li_el_new =  $('.emails-box li[data-val="'+email+'"]');
        
        $.ajax({
    		type: "POST",          
    		url: url+"/ajax/remove-email.php",
    		data: 'email='+email,
    		success: function(data) {
    		    
    		    
    		    li_el_new.remove();
    		  
    		},
    		error: function () {
    		    
    		    alert("Произошла ошибка!");    
    		    
    		}
    	});
            
        
        
    });
    
}






//Добавить нотификацию
function addNotif ()  {
    
    var url = urlWithProtocol ();
    
    var msg = jQuery('#add-notif').serialize();
    
    
    $.ajax({
		type: "POST",          
		url: url+"/ajax/add-notif.php",
		data: msg,
		success: function(data) {
		    
		     location.reload(true);

		},
		error: function () {
		    
		    alert("Произошла ошибка!");    
		    
		}
	});
    
    
}





function removeNotif () {
    
    $('.notif-box .center-item li .remove').click(function(){
        
        var url = urlWithProtocol ();
    
        var center = $(this).parent().attr('data-center');
        
        
        li_el_new = $(this).parent();
        
        var notif = $(this).parent().attr('data-name');
        
        
        $.ajax({
    		type: "POST",          
    		url: url+"/ajax/remove-notif.php",
    		data: 'center='+center+'&notif='+notif,
    		success: function(data) {
    		    
    		    console.log(data);
    		    li_el_new.remove();
    		  
    		},
    		error: function () {
    		    
    		    alert("Произошла ошибка!");    
    		    
    		}
    	});
        
        
        
    });
     
   
    
}




//Добавить нотификацию для regexp
function addRegData ()  {
    
    var url = urlWithProtocol ();
    
    var msg = jQuery('#add-reg-data').serialize();
    
    
    $.ajax({
		type: "POST",          
		url: url+"/ajax/add-reg-notif.php",
		data: msg,
		success: function(data) {
		    
		    
		     location.reload(true);

		},
		error: function () {
		    
		    alert("Произошла ошибка!");    
		    
		}
	});
    
    
}





function removeRegNotif () {
    
    $('.notif-reg-box .center-item li .remove').click(function(){
        
        var url = urlWithProtocol ();
    
        var reg = $(this).parent().attr('data-key');
        
        
        li_el_new = $(this).parent();
        
        
        $.ajax({
    		type: "POST",          
    		url: url+"/ajax/remove-reg-notif.php",
    		data: 'reg='+reg,
    		success: function(data) {


    		    li_el_new.remove();
    		  
    		},
    		error: function () {
    		    
    		    alert("Произошла ошибка!");    
    		    
    		}
    	});
        
        
        
    });
     
   
    
}

