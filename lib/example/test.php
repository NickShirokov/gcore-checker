<?php
// example of how to use basic selector to retrieve HTML contents
include('../simple_html_dom.php');
 

$html = str_get_html('<div id="hello">Hello</div><div id="world">World</div>');
$html->find('div', 1)->class = 'bar';
$html->find('div[id=world]', 0)->innertext = 'foo';
echo $html;






?>