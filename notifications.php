<?php 

include('html/header.php'); 


?>



<section>
    
    <div class="container">
        <h2>Notifications</h2>
        
        
       
       <div class="emails-box">
           
           <ul>
               <?php 
               
                   $content = '';
                   
               
                   $arr = json_decode(file_get_contents('json/notif-emails.json'),true);
               
               
                   if (!empty($arr)) {
                       
                       
                       foreach ($arr as $key => $value) {
                           
                           
                           $content.= " <li data-val=$value>$value<span class='remove'>X</span></li>";
                       }
                       
                   } 
               
                   
                   echo $content;
                   
               ?>
               
               
               
               
               
           </ul>
           
           
       </div>
       
         
        <form id="add-email" class="form-signin" onsubmit="addEmail(); return false;" method="POST">
        <h3>Email settings</h3>
          
     
          <input id="email" type="email" class="form-control" required name="email" value="">
                    
          
          
          <button class="btn btn-lg btn-primary">add email</button>
          
          
        </form>   
        
       
       
       
       
           <div class="notif-box">
               
               
               <?php 
               
               
                   $content = '';
                   
               
                   $arr = json_decode(file_get_contents('json/notif-servers.json'),true);
               
                   
                   
                   $main_arr = json_decode(file_get_contents('servers.json'),true);
                   
         
                   if (!empty($arr)) {
                       
                       foreach ($arr as $key => $value) {
                           
                           if (array_key_exists($key, $main_arr)) {
                               
                               
                               $center = $key; 
                               
                               $content.= '<div class="center-item" data-number='.$key.'>';
                           
                               $content.= '<h2>'.$key.' '.$main_arr[$key]['name'].'</h2>';
                               
                               if (!empty($value['data'])) {
                                   
                                   $data = $value['data'];
                                   
                                   
                                   
                                   $content.= '   <ul>';
                                   foreach ($data as $key => $value) {
                                       
                                       $content.= '
                                                           <li data-center='.$center.' data-name="'.$value['name'].'">
                                     
                                                                <span class="name">'.$value['name'].'</span>
                                                                <span class="count">'.$value['count'].'</span>
                                                                <span class="remove">X</span>
                                                               
                                                            </li>
                                                           ';
                                                           

                                       
                                   }
                                   
                                   $content.= '</ul>';
                                   
                               }
                               
                               
                               
                               
                               $content.= '</div>';
                           }
                       } 
                       
                   }
               
                   echo $content;
               
               ?>
               
               
               
            
               
               
           </div>
       
       
       
                
        <form id="add-notif" class="form-signin" onsubmit="addNotif();return false;" method="POST">
            <h3>Add notification for server type</h3>
          
           <label>Datacenter number</label>
              <input id="" type="number" class="form-control" required name="center_index" value="">
           
          
          
              <label>Server type</label>
              <textarea class="form-control" name="server_name"></textarea>
          
              
   
                  
                  <label>Condition less than</label>
              <input id="" type="number" class="form-control" required name="count" value="">
     
          
          
          <button class="btn btn-lg btn-primary">Add</button>
          
          
        </form>   
       
    </div>
    
    
    
</section>



<?php include('html/footer.php');  ?>
