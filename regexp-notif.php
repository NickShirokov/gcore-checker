<?php 

include('html/header.php'); 


?>



<section>
    
    <div class="container">
        <h2>Reg Exp Notifications</h2>
        
        
            <ul>
               
               <?php 
                 $arr = json_decode(file_get_contents('servers.json'),true);
                 
                 
                 foreach ($arr as $key => $value) {
                     
                     echo '<li>'.$key.' '.$value['name'].'</li>';
                     
                 }
                 
               
               ?>
               
               
           </ul>
        
        
        
        <form id="add-reg-data" class="form-signin" onsubmit="addRegData(); return false;" method="POST">
        <h3>Regexp conditions</h3>
          
         <label>Enter your search phrase - min 2 symbols</label>
          <input id="regexp" type="text" class="form-control" minlength="2" required name="regexp" value="" placeholder="Search phrase">
                    
            <label>Enter numbers of data centers. <b>For example: <span>1,2,7</span></b></label>        
           <input id="datacenters" type="text" name="datacenters" class="form-control" required placeholder="Enter datacenters">
          
          
           <label>Enter count</label>        
           <input id="count" type="text" name="count" class="form-control" required placeholder="Enter count">
          
          <label>Enter name condition</label>        
           <input id="name" type="text" name="name" class="form-control" required placeholder="Enter name">
          
          
          
          <button class="btn btn-lg btn-primary">Add condition</button>
          
        </form>   
 
    </div>
    
    <div class="container">
        <h2>Regexp List</h2>
        
       <div class="notif-reg-box">
           
           <div class="center-item">
           
       
           
           
           
           <ul>
               <?php 
               
                   $content = '';
                   
               
                   $arr = json_decode(file_get_contents('json/reg-notif.json'),true);
                   
               
            
               
                   if (!empty($arr)) {
                       
                       
                       foreach ($arr as $key => $value) {
                           
                           
                           
                           $centers = implode(',',$value['centers']);
                          
                           
                           
                           $content.= '<li  data-key="'.$key.'">
                                          <span class="name">'.$key.'</span>
                                            <span class="count">'.$centers.'</span>
                                            
                                            
                                            
                                            <span class="count" style="margin-left: 40px;">'.$value['count'].'</span>
                                            <span class="remove">X</span>
                                            
                                            <p>'.$value['name'].'</p>
                                       </li>';
                       }
                       
                   } 
               
                   
                   echo $content;
                   
                   
                   
               ?>
               
               
               
               
               
           </ul>
           </div>
           
       </div>
    
    
    </div>
    
    
</section>



<?php include('html/footer.php');  ?>