<?php

$cat_arr = array(
                'name' => 'Cat'
            );

$lion_arr = array(
                'name' => 'Lion'
                );

/**
 * Class Animal, со времен первой редакции книги прошло более 20 лет и этот паттерн немного эволюционировал,
 * и теперь всегда используют его сокращенную форму
 */
abstract class Animal {
    // фабричный метод, который на основе типа возвращает объект
    public static function initial(array $animal_arr) {
        
        $animal =  $animal_arr['name'];
         
        //return new $animal();
        
        return new $animal();
        
    }
    
    abstract public function voice();
}



class Lion extends Animal {
    public function voice() {
        echo 'Rrrrrrrr i\'m the lion <br />' . PHP_EOL;
    }
}



class Cat extends Animal {
    public function voice() {
        echo 'Meow, meow i\'m the kitty <br />' . PHP_EOL;
    }
}

$animal1 = Animal::initial($lion_arr);
$animal2 = Animal::initial($cat_arr);

$animal1->voice();
$animal2->voice();


?>